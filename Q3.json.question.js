const givenString = `
    {
        "_id": "a",
        "user": "bryan",
        "groups":{
            "shelton":[
                "maya",
                "nupur"
            ],
            "cebu":[
                "nicko",
                "jeffery"
            ]
        }
    }
`;

console.log(JSON.parse(givenString)['user']);
console.log(JSON.parse(givenString)['groups']['shelton'][0]);
console.log("Number of people in Cebu:", JSON.parse(givenString)['groups']['cebu'].length);