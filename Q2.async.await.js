'use strict';


//Three different files for input

const file1 = 'file1.txt';
const file2 = 'file2.txt';
const file3 = 'file3.txt';


//function to read file based off of the example provided in the code. I tried to use async/await instead of Promise but thought this worked better and was more readable.

const readFile = async (filePath)=>{
    return new Promise(resolve => setTimeout(()=>{
        console.log("Done Loading"+filePath);
        resolve(filePath);
    },Math.random()*5000));
};


//function to print the output message.

const output = (msg)=>{
    console.log("running:"+msg);
};

//Wrapped our output call in asynchronous function to await one file load after another.
const running= async ()=>{
    const p1 = await readFile(file1);
    const p2 = await readFile(file2);
    const p3 = await readFile(file3);

    await console.log("Done all loading");


    await output(p1);
    await output(p2);
    await output(p3);
    await console.log("All files executed");
};

//calling our wrapper function

running();



// const p2 = readFile(file2);
// const p3 = readFile(file3);


// const p1 = readFile(file1);
//
// p1.then((value)=>{
//     output(value);
// });

