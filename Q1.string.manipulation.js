'use strict';


//Given String 1

const givenString1 = `
id,user
a,Bryan
b,Maya
c,Nupur
d,Jefferey
e,Nicko
`;

//Given String 2

const givenString2 = `
id,accessLevel
a,1
b,2
c,1
d,2
e,1
`;


/* Generic Function that parses given string. It uses the first line as header keys in an object and rest of the string as a key value pairs*/

const StringParser = (givenString)=>{

    //Splitting string on linebreak

    const parsedString = givenString.trim().split('\n');

    // Despite the header being fixed, This part ensures that in case of the change in header in future, the code is robust enough to handle it.

    const key1 = parsedString[0].split(',')[0];
    const key2 = parsedString[0].split(',')[1];

    //removing the header from the actual values which can then be mapped as an object

    parsedString.splice(0,1);

    //initializing a generic list that collects all the parsed objects before returning to user

    const returnList =[];

    // Looping over the array of splitted strings and mapping them to their respective key and values.
    parsedString.forEach((individual)=> {
        const individualObject = {};

        individualObject[key1] = individual.split(',')[0];
        individualObject[key2] = individual.split(',')[1];

        returnList.push(individualObject);

    });
    return returnList

};

//Calling the function we wrote above to parse our string and give us objects

const users = StringParser(givenString1);
const accessList = StringParser(givenString2);

//Mapping Each individual access id 1 with their respective user and printing out the result in console.

accessList.forEach((access)=>{
        if(parseInt(access.accessLevel)===1){
            users.forEach((user)=>{
                if(user.id===access.id){
                    console.log(user.user+','+access.accessLevel);
                }
            })
        }
    });

